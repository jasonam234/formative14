-- Updating production database's table
USE production;

CREATE TABLE Brand(
	id 		INTEGER UNIQUE AUTO_INCREMENT,
    name    VARCHAR(30),
    
    PRIMARY KEY(id)
);

SET SQL_SAFE_UPDATES = 0;
DELETE FROM Product;
ALTER TABLE Product CHANGE brand brand_id INTEGER NOT NULL;
ALTER TABLE Product ADD FOREIGN KEY (brand_id) REFERENCES Brand(id);

SELECT * FROM Product;