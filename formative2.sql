-- For Staging Database  
USE staging;
CREATE TABLE Product_ClientA(
	id 			INTEGER UNIQUE AUTO_INCREMENT,
    name 		VARCHAR(40),
    description VARCHAR(100),
    artnumber 	VARCHAR(20),
    price 		INTEGER,
    stock 		INTEGER,
    deleted 	BOOLEAN,
    brand_name 	VARCHAR(30),
    
    PRIMARY KEY(id)
);
SELECT * FROM Product_ClientA;

CREATE TABLE Product_ClientB(
	id 				  INTEGER UNIQUE AUTO_INCREMENT,
    nama 		      VARCHAR(40),
    deskripsi 	   	  VARCHAR(100),
    nomor_artikel 	  VARCHAR(20),
    harga 			  INTEGER,
    persediaan_barang INTEGER,
    dihapus 	      BOOLEAN,
    nama_merek 		  VARCHAR(30),
    
    PRIMARY KEY(id)
);
SELECT * FROM Product_ClientB;

-- For Production Database 
USE production;
CREATE TABLE Product(
	id 			INTEGER UNIQUE AUTO_INCREMENT,
    name 		VARCHAR(40),
    description VARCHAR(100),
    artnumber 	VARCHAR(20),
    price 		INTEGER,
    stock 		INTEGER,
    deleted 	BOOLEAN,
    brand 		VARCHAR(30),
    
    PRIMARY KEY(id)
);
SELECT * FROM Product;