-- Inserting data into staging tables 
USE staging;
INSERT INTO Product_ClientA (name, description, artnumber, price, stock, deleted, brand_name) 
VALUES
("Iphone 5", "5th Generation Iphone", "A-001", 3500000, 50, 0, "Apple"),
("Iphone 5S", "5th Generation Iphone with upgraded hardware", "A-002", 4000000, 150, 0, "Apple"),
("Iphone 6", "6th Generation Iphone", "A-003", 4500000, 300, 0, "Apple"),
("Iphone 6S", "6th Generation Iphone with upgraded hardware", "A-004", 5000000, 500, 0, "Apple"),
("Iphone 7", "7th Generation Iphone", "A-005", 7000000, 600, 0, "Apple"),
("Samsung S7", "Refurbished Samsung S7", "A-006", 4000000, 20, 0, "Samsung"),
("Samsung S8", "Second hand Samsung S8", "A-007", 4500000, 50, 0, "Samsung"),
("Samsung S9", "Refurbished Samsung S9", "A-008", 5000000, 100, 0, "Samsung"),
("Samsung A51", "Midrange Samsung phone", "A-009", 3000000, 10, 0, "Samsung"),
("Samsung A52", "Newest Midrange Samsung phone", "A-010", 4000000, 20, 0, "Samsung");
SELECT * FROM Product_ClientA;

INSERT INTO Product_ClientB (nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek) 
VALUES
("Playstation 2", "Refurbished Playstation 2", "B-001", 1500000, 10, 0, "Sony"),
("Playstation 3", "Refurbished Playstation 3", "B-002", 2000000, 90, 0, "Sony"),
("Playstation 4", "New Playstation 4", "B-003", 4000000, 100, 0, "Sony"),
("Playstation 5", "New Playstation 4", "B-004", 12000000, 1, 0, "Sony"),
("XBOX 360", "Refurbished XBOX 360", "B-005", 3000000, 20, 0, "Microsoft"),
("XBOX One", "New XBOX One", "B-006", 5000000, 30, 0, "Microsoft"),
("Nintendo Switch", "New Nintendo Switch", "B-007", 4000000, 50, 0, "Nintendo"),
("Nintendo Switch OLED", "New Nintendo Switch OLED", "B-008", 6000000, 30, 0, "Nintendo"),
("Oculus Rift", "New pair of VR Headset", "B-009", 10000000, 10, 0, "Oculus"),
("Logitech G102", "Refurbished Logitech G102 Mouse", "B-010", 100000, 100, 0, "Logitech");
SELECT * FROM Product_ClientB;